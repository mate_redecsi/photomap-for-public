import android.support.v7.app.AppCompatActivity;

/**
 * Base class for all activities in the application.
 * Created by Mate Redecsi on 2016. 01. 16.
 */
public class BaseActivity extends AppCompatActivity {
}
